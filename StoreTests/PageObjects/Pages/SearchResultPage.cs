﻿using OpenQA.Selenium;
using StoreTests.PageObjects.Sections;

namespace StoreTests.PageObjects.Pages
{
    class SearchResultPage
    {
        private readonly IWebDriver _driver;
        public ProductListSection ProductList { get; }
        public IWebElement SearchPhrase => _driver.FindElement(By.CssSelector(".page-heading span.lighter"));

        public SearchResultPage(IWebDriver driver)
        {
            _driver = driver;
            ProductList = new ProductListSection(driver);
        }
    }
}
