﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using StoreTests.PageObjects.Sections;

namespace StoreTests.PageObjects.Pages
{
    class ShippingPage
    {
        private readonly IWebDriver _driver;
        public OrderStepSection OrderStepSection { get; }
        private IWebElement ProceedToCheckoutButton => _driver.FindElement(By.Name("processCarrier"));
        private IWebElement TermsCheckbox => _driver.FindElement(By.Id("cgv"));

        public ShippingPage(IWebDriver driver)
        {
            _driver = driver;
            OrderStepSection = new OrderStepSection(_driver);
        }

        public PaymentPage ProceedToPayment()
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => ProceedToCheckoutButton.Displayed);

            ProceedToCheckoutButton.Click();
            return new PaymentPage(_driver);
        }

        public void AgreeTerms()
        {
            TermsCheckbox.Click();
        }
    }
}
