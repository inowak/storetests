﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using StoreTests.PageObjects.Sections;

namespace StoreTests.PageObjects.Pages
{
    class AddressPage
    {
        private readonly IWebDriver _driver;
        public OrderStepSection OrderStepSection { get; }
        private IWebElement ProceedToCheckoutButton => _driver.FindElement(By.Name("processAddress"));
        private IWebElement AddressDropdown => _driver.FindElement(By.Id("id_address_delivery"));

        public AddressPage(IWebDriver driver)
        {
            _driver = driver;
            OrderStepSection = new OrderStepSection(_driver);
        }

        public ShippingPage ProceedToShipping()
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => ProceedToCheckoutButton.Displayed);

            ProceedToCheckoutButton.Click();
            return new ShippingPage(_driver);
        }

        public string GetSelectedAddressName()
        {
            return AddressDropdown.Text.Trim();
        }
    }
}
