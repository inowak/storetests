﻿using System;
using OpenQA.Selenium;

namespace StoreTests.PageObjects.Pages
{
    class SignInPage
    {
        private readonly IWebDriver _driver;
        private IWebElement SignUpEmailInput => _driver.FindElement(By.Id("email_create"));
        private IWebElement SignUpButton => _driver.FindElement(By.Id("SubmitCreate"));

        public SignInPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public AccountCreationPage CreateAnAccount(string email)
        {
            var login = $"{DateTime.UtcNow:yyyyMMddHHmmssfff}-{email}";
            SignUpEmailInput.SendKeys(login);
            SignUpButton.Click();

            return new AccountCreationPage(_driver);
        }

    }
}
