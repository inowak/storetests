﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace StoreTests.PageObjects.Pages
{
    class AccountCreationPage
    {
        private readonly IWebDriver _driver;
        private IWebElement MrRadioButton => _driver.FindElement(By.Id("id_gender1"));
        private IWebElement MrsRadioButton => _driver.FindElement(By.Id("id_gender2"));
        private IWebElement PersonalFirstNameInput => _driver.FindElement(By.Id("customer_firstname"));
        private IWebElement PersonalLastNameInput => _driver.FindElement(By.Id("customer_lastname"));
        private IWebElement PasswordInput => _driver.FindElement(By.Id("passwd"));
        private IWebElement DayOfBirthDropdown => _driver.FindElement(By.Id("days"));
        private IWebElement MonthOfBirthDropdown => _driver.FindElement(By.Id("months"));
        private IWebElement YearOfBirthDropdown => _driver.FindElement(By.Id("years"));
        private IWebElement NewsletterCheckbox => _driver.FindElement(By.Id("newsletter"));
        private IWebElement OptInCheckbox => _driver.FindElement(By.Id("optin"));
        private IWebElement CompanyInput => _driver.FindElement(By.Id("company"));
        private IWebElement AddressInput => _driver.FindElement(By.Id("address1"));
        private IWebElement Address2Input => _driver.FindElement(By.Id("address2"));
        private IWebElement CityInput => _driver.FindElement(By.Id("city"));
        private IWebElement StateDropdown => _driver.FindElement(By.Id("id_state"));
        private IWebElement ZipCodeInput => _driver.FindElement(By.Id("postcode"));
        private IWebElement CountryDropdown => _driver.FindElement(By.Id("id_country"));
        private IWebElement AdditionalInformationTextArea => _driver.FindElement(By.Id("other"));
        private IWebElement HomePhoneInput => _driver.FindElement(By.Id("phone"));
        private IWebElement MobilePhoneInput => _driver.FindElement(By.Id("phone_mobile"));
        private IWebElement AddressAliasInput => _driver.FindElement(By.Id("alias"));
        private IWebElement RegisterButton => _driver.FindElement(By.Id("submitAccount"));

        public AccountCreationPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public void FillInPersonalInformation(bool isWoman, string firstName, string lastName,
            string password, string day, string month, string year, bool newsletterPermission, bool optInPermission)
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => PersonalFirstNameInput.Displayed);

            IsWomanOption(isWoman);
            PersonalFirstNameInput.SendKeys(firstName);
            PersonalLastNameInput.SendKeys(lastName);
            PasswordInput.SendKeys(password);
            SelectDateOfBirth(day, month, year);
            NewsletterSignUp(newsletterPermission);
            OptIn(optInPermission);
        }

        public void FillInAddress(string company, string address1, string address2,
            string city, string state, string zipCode, string country, string additionalInfo, string homeNumber,
            string mobilePhone, string alias)
        {
            CompanyInput.SendKeys(company);
            AddressInput.SendKeys(address1);
            Address2Input.SendKeys(address2);
            CityInput.SendKeys(city);
            SelectState(state);
            ZipCodeInput.SendKeys(zipCode);
            SelectCountry(country);
            AdditionalInformationTextArea.SendKeys(additionalInfo);
            HomePhoneInput.SendKeys(homeNumber);
            MobilePhoneInput.SendKeys(mobilePhone);
            AddressAliasInput.Clear();
            AddressAliasInput.SendKeys(alias);
        }

        public MyAccountPage ClickRegister()
        {
            RegisterButton.Click();
            return new MyAccountPage(_driver);
        }

        private void IsWomanOption(bool isWoman)
        {
            if (isWoman)
            {
                MrsRadioButton.Click();
            }
            else
            {
                MrRadioButton.Click();
            }
        }
        private void OptIn(bool optInPermission)
        {
            if (optInPermission)
            {
                OptInCheckbox.Click();
            }
        }

        private void NewsletterSignUp(bool newsletterPermission)
        {
            if (newsletterPermission)
            {
                NewsletterCheckbox.Click();
            }
        }

        private void SelectDateOfBirth(string day, string month, string year)
        {
            var dayOfBirth = new SelectElement(DayOfBirthDropdown);
            dayOfBirth.SelectByValue(day);

            var monthOfBirth = new SelectElement(MonthOfBirthDropdown);
            monthOfBirth.SelectByValue(month);

            var yearOfBirth = new SelectElement(YearOfBirthDropdown);
            yearOfBirth.SelectByValue(year);
        }

        private void SelectCountry(string selectedCountry)
        {
            var country = new SelectElement(CountryDropdown);
            country.SelectByText(selectedCountry);
        }

        private void SelectState(string selectedState)
        {
            var state = new SelectElement(StateDropdown);
            state.SelectByText(selectedState);
        }
    }
}
