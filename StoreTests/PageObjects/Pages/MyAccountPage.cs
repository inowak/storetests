﻿using OpenQA.Selenium;
using StoreTests.PageObjects.Sections;

namespace StoreTests.PageObjects.Pages
{
    class MyAccountPage
    {
        private readonly IWebDriver _driver;
        private IWebElement OrderHistoryButton => _driver.FindElement(By.CssSelector("a[title='Orders']"));
        public MenuSection UserMenuSection { get; }

        public MyAccountPage(IWebDriver driver)
        {
            _driver = driver;
            UserMenuSection = new MenuSection(driver);
        }

        public OrderHistoryPage ClickOrderHistory()
        {
            OrderHistoryButton.Click();
            return new OrderHistoryPage(_driver);
        }
    }
}
