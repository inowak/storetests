﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using StoreTests.PageObjects.Sections;

namespace StoreTests.PageObjects.Pages
{
    class ProductPage
    {
        private readonly IWebDriver _driver;
        private IWebElement AddToCartButton => _driver.FindElement(By.CssSelector("#add_to_cart button"));
        public MenuSection UserMenuSection { get; }

        public ProductPage(IWebDriver driver)
        {
            _driver = driver;
            UserMenuSection = new MenuSection(driver);
        }

        public AddProductConfirmationPopUp AddProduct()
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => AddToCartButton.Displayed);

            AddToCartButton.Click();
            return new AddProductConfirmationPopUp(_driver);
        }
    }
}
