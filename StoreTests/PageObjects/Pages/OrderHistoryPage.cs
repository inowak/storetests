﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace StoreTests.PageObjects.Pages
{
    class OrderHistoryPage
    {
        private readonly IWebDriver _driver;

        public IList<IWebElement> ProductLinks => _driver.FindElements(By.CssSelector("tbody tr"));
        private IWebElement OrderTable => _driver.FindElement(By.Id("order-list"));
        public OrderHistoryPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public bool IsOrderVisible()
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => OrderTable.Displayed);  

            return ProductLinks.Count > 0;
        }
    }
}
