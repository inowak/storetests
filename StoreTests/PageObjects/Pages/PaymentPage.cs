﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using StoreTests.PageObjects.Sections;

namespace StoreTests.PageObjects.Pages
{
    class PaymentPage
    {
        private readonly IWebDriver _driver;
        public OrderStepSection OrderStepSection { get; }
        public MenuSection UserMenuSection { get; }
        private IWebElement BankWireOptionLink => _driver.FindElement(By.ClassName("bankwire"));
        private IWebElement ConfirmOrderButton => _driver.FindElement(By.CssSelector("#cart_navigation > button"));
        private IWebElement PaymentConfirmation => _driver.FindElement(By.ClassName("cheque-indent"));

        public PaymentPage(IWebDriver driver)
        {
            _driver = driver;
            OrderStepSection = new OrderStepSection(driver);
            UserMenuSection = new MenuSection(driver);
        }

        public void SelectBankWirePaymentOption()
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => BankWireOptionLink.Displayed);

            BankWireOptionLink.Click();
        }

        public void ConfirmOrder()
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => ConfirmOrderButton.Displayed);

            ConfirmOrderButton.Click();
        }

        public string GetPaymentConfirmation()
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => PaymentConfirmation.Displayed);

            return PaymentConfirmation.Text;
        }
    }
}
