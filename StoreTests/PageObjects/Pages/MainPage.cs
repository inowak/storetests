﻿using OpenQA.Selenium;
using StoreTests.PageObjects.Sections;

namespace StoreTests.PageObjects.Pages
{
    class MainPage
    {
        private readonly IWebDriver _driver;
        public MenuSection UserMenuSection { get; }

        public MainPage(IWebDriver driver)
        {
            _driver = driver;
            UserMenuSection = new MenuSection(driver);
        }
    }
}
