﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using StoreTests.PageObjects.Sections;

namespace StoreTests.PageObjects.Pages
{
    class CartPage
    {
        private readonly IWebDriver _driver;
        public OrderStepSection OrderStepSection { get; }
        public MenuSection UserMenuSection { get; }
        private IWebElement ProceedToCheckoutButton => _driver.FindElement(By.ClassName("standard-checkout"));

        public CartPage(IWebDriver driver)
        {
            _driver = driver;
            OrderStepSection = new OrderStepSection(_driver);
            UserMenuSection = new MenuSection(driver);
        }

        public AddressPage ProceedToAddress()
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => ProceedToCheckoutButton.Displayed);

            ProceedToCheckoutButton.Click();
            return new AddressPage(_driver);
        }
    }
}
