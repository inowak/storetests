﻿using OpenQA.Selenium;

namespace StoreTests.PageObjects.Sections
{
    class OrderStepSection
    {
        private readonly IWebDriver _driver;
        private IWebElement CurrentStep => _driver.FindElement(By.ClassName("step_current"));
        public OrderStepSection(IWebDriver driver)
        {
            _driver = driver;
        }

        public string GetCurrentStepName()
        {
            return CurrentStep.Text;
        }
    }
}
