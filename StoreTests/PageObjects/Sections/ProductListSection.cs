﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using StoreTests.PageObjects.Pages;

namespace StoreTests.PageObjects.Sections
{
    class ProductListSection
    {
        private readonly IWebDriver _driver;
        public IList<IWebElement> ProductLinks => _driver.FindElements(By.CssSelector(".product_list a.product-name"));
        public ProductListSection(IWebDriver driver)
        {
            _driver = driver;
        }

        public ProductPage ClickOnRandomProduct()
        {
            var random = new Random();
            ProductLinks[random.Next(0, ProductLinks.Count-1)].Click();

            return new ProductPage(_driver);
        }
    }

}
