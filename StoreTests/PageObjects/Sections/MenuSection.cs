﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using StoreTests.PageObjects.Pages;

namespace StoreTests.PageObjects.Sections
{
    class MenuSection
    {
        private readonly IWebDriver _driver;
        private IWebElement SignInLink => _driver.FindElement(By.CssSelector("a.login"));
        private IWebElement MyAccountLink => _driver.FindElement(By.CssSelector("a.account"));
        private IWebElement LogOutLink => _driver.FindElement(By.CssSelector("a.logout"));
        private IWebElement SearchInput => _driver.FindElement(By.ClassName("search_query"));
        private IWebElement SearchButton => _driver.FindElement(By.ClassName("button-search"));
        private IWebElement EmptyCartSpan => _driver.FindElement(By.ClassName("ajax_cart_no_product"));

        public MenuSection(IWebDriver driver)
        {
            _driver = driver;
        }

        public SignInPage ClickSignIn()
        {
            SignInLink.Click();

            return new SignInPage(_driver);
        }

        public string GetUserName()
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => MyAccountLink.Displayed);

            return MyAccountLink.Text;
        }

        public bool IsSignOutLinkVisible()
        {
            return LogOutLink.Displayed;
        }

        public SearchResultPage Search(string phrase)
        {
            SearchInput.SendKeys(phrase);
            SearchButton.Click();

            return new SearchResultPage(_driver);
        }

        public MyAccountPage ClickCustomerAccount()
        {
            MyAccountLink.Click();

            return new MyAccountPage(_driver);
        }

        public bool IsCartEmpty()
        {
            return EmptyCartSpan.Displayed;
        }
    }
}
