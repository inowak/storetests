﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using StoreTests.PageObjects.Pages;

namespace StoreTests.PageObjects.Sections
{
    class AddProductConfirmationPopUp
    {
        private readonly IWebDriver _driver;
        private IWebElement SuccessMessage => _driver.FindElement(By.CssSelector(".layer_cart_product h2"));
        private IWebElement ProceedCheckoutToButton => _driver.FindElement(By.ClassName("button-medium"));

        public AddProductConfirmationPopUp(IWebDriver driver)
        {
            _driver = driver;
        }

        public string GetMessage()
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(d => SuccessMessage.Displayed);

            return SuccessMessage.Text;
        }

        public CartPage ClickProceedToCheckout()
        {
            ProceedCheckoutToButton.Click();
            return new CartPage(_driver);
        }
    }
}
