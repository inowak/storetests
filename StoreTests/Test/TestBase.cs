﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace StoreTests.Test
{
    [TestFixture]
    public class TestBase
    {
        protected IWebDriver Driver;

        [SetUp]
        public void Start()
        {
            Driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("http://automationpractice.com/index.php");
        }

        [TearDown]
        public void Stop()
        {
            Driver.Quit();
        }
    }
}
