﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using StoreTests.PageObjects.Pages;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace StoreTests.Test
{
    [TestFixture]
    [TestClass]
    public class OrderTest : TestBase
    {
        [Test]
        public void BuySearchedProductAsRegisteredUser()
        {
            // Create an account
            var mainPage = new MainPage(Driver);
            var loginPage = mainPage.UserMenuSection.ClickSignIn();
            var accountCreationPage = loginPage.CreateAnAccount("anna@test.pl");
            accountCreationPage.FillInPersonalInformation(true, "Anna", "Kowalska", "Password123", "13", "11", "1988", true, true);
            accountCreationPage.FillInAddress( "ABC", "Rose Street 3",
                "Building D", "New York", "Colorado", "55555", "United States", "test", "+48639483029", "+489283749203", "Home address");
            var myAccountPage = accountCreationPage.ClickRegister();

            Assert.AreEqual("Anna Kowalska", myAccountPage.UserMenuSection.GetUserName());
            Assert.AreEqual(true, myAccountPage.UserMenuSection.IsSignOutLinkVisible());

            // Search product
            var searchResultPage = myAccountPage.UserMenuSection.Search("Dress");

            Assert.AreEqual("\"DRESS\"", searchResultPage.SearchPhrase.Text);

            // Add product to cart
            var productPage = searchResultPage.ProductList.ClickOnRandomProduct();
            var addProductConfirmationPopUp = productPage.AddProduct();

            Assert.AreEqual("Product successfully added to your shopping cart", addProductConfirmationPopUp.GetMessage());

            // Step 1 -> Cart
            var cartPage = addProductConfirmationPopUp.ClickProceedToCheckout();

            Assert.AreEqual("01. Summary", cartPage.OrderStepSection.GetCurrentStepName());
            Assert.IsFalse(cartPage.UserMenuSection.IsCartEmpty(), "Cart is Empty");

            // Step 3 -> Address
            var addressPage =  cartPage.ProceedToAddress();

            Assert.AreEqual("03. Address", addressPage.OrderStepSection.GetCurrentStepName());
            Assert.AreEqual("Home address", addressPage.GetSelectedAddressName());

            // Step 4 -> Shipping
            var shippingPage = addressPage.ProceedToShipping();
            shippingPage.AgreeTerms();

            Assert.AreEqual("04. Shipping", addressPage.OrderStepSection.GetCurrentStepName());
            
            //Step 5 -> Payment
            var paymentPage = shippingPage.ProceedToPayment();

            Assert.AreEqual("05. Payment", addressPage.OrderStepSection.GetCurrentStepName());

            // Pick payment
            paymentPage.SelectBankWirePaymentOption();

            Assert.AreEqual("05. Payment", addressPage.OrderStepSection.GetCurrentStepName());
            Assert.AreEqual("You have chosen to pay by bank wire. Here is a short summary of your order:", paymentPage.GetPaymentConfirmation());

            //Order Confirmation
            paymentPage.ConfirmOrder();

            Assert.AreEqual("Your order on My Store is complete.", paymentPage.GetPaymentConfirmation());
            Assert.IsTrue(paymentPage.UserMenuSection.IsCartEmpty(), "Cart is not empty");

            //Check my account
            myAccountPage = paymentPage.UserMenuSection.ClickCustomerAccount();
            var orderHistoryPage = myAccountPage.ClickOrderHistory();

            Assert.IsTrue(orderHistoryPage.IsOrderVisible(), "Order is not visible");


        }

    }
}
